/*===============================================================================
Copyright (c) 2020 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

package com.mbds.mamac

import android.app.Activity
import android.content.DialogInterface
import android.content.res.AssetManager
import android.opengl.GLSurfaceView
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.core.view.GestureDetectorCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import org.imaginativeworld.whynotimagecarousel.CarouselItem
import org.imaginativeworld.whynotimagecarousel.CarouselOnScrollListener
import org.imaginativeworld.whynotimagecarousel.ImageCarousel
import org.imaginativeworld.whynotimagecarousel.OnItemClickListener
import java.nio.ByteBuffer
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import kotlin.concurrent.schedule

data class CarouselItemData(val name: String, val description: String) {}

/**
 * Activity to demonstrate how to use Vuforia Image Target and Model Target features,
 * Video Background rendering and Vuforia lifecycle.
 */
class VuforiaActivity : AppCompatActivity(), GLSurfaceView.Renderer, SurfaceHolder.Callback {

    private lateinit var mGLView: GLSurfaceView
    private lateinit var carousel: ImageCarousel
    private lateinit var selectedDrawable: ImageView

    private var selectedItem: CarouselItem? = null
    private var alertDialog: AlertDialog? = null

    private var mTarget = 0
    private var mProgressIndicatorLayout: RelativeLayout? = null

    private var mWidth = 0
    private var mHeight = 0

    private var mVuforiaStarted = false
    private var mSurfaceChanged = false

    private var mGestureDetector: GestureDetectorCompat? = null

    private val imageTargetUniqueIds = arrayOf(
        "46d3db7a09a9484ba889146eb084401b",
        "967e8834c8e746babbb8441c4f33a62b"
    );

    private val list = mutableListOf<CarouselItem>()

    private val imageData = mutableMapOf<String, CarouselItemData>()

    // Native methods
    external fun initAR(activity: Activity, assetManager: AssetManager, target: Int)
    external fun deinitAR()

    external fun startAR(): Boolean
    external fun stopAR()

    external fun pauseAR()
    external fun resumeAR()

    external fun cameraPerformAutoFocus()
    external fun cameraRestoreAutoFocus()

    external fun initRendering()
    external fun addTextures(
        imgWidth: Int,
        imgHeight: Int,
        imgBytes: ByteBuffer,
        imageUniqueId: String
    )
    external fun deinitRendering()
    external fun configureRendering(width: Int, height: Int, orientation: Int): Boolean
    external fun renderFrame(): Boolean
    external fun isTapOnScreenInsideTarget(targetUniqueId: String, x: Float, y: Float): Boolean
    external fun addToDiscoveredItemList(targetUniqueId: String)
    external fun isIdInDiscoveredItemList(targetUniqueId: String): Boolean


    // Activity methods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vuforia)

        // Create dialog
        createDialog()

        imageData["46d3db7a09a9484ba889146eb084401b"] = CarouselItemData("La Joconde", "La Joconde, ou Portrait de Mona Lisa, est un tableau de l'artiste Léonard de Vinci, réalisé entre 1503 et 1506 ou entre 1513 et 15161,2, et peut-être jusqu'à 1519 (l'artiste étant mort cette année-là, le 2 mai)3, qui représente un portrait mi-corps, probablement celui de la Florentine Lisa Gherardini, épouse de Francesco del Giocondo. Acquise par François Ier, cette peinture à l'huile sur panneau de bois de peuplier de 77 × 53 cm est exposée au musée du Louvre à Paris. La Joconde est l'un des rares tableaux attribués de façon certaine à Léonard de Vinci.")
        imageData["967e8834c8e746babbb8441c4f33a62b"] = CarouselItemData("Guernica", "Picasso réalisa cette huile sur toile de style cubiste entre le 1er mai et le 4 juin 1937, à Paris, en réponse à une commande du gouvernement républicain de Francisco Largo Caballero pour le pavillon espagnol de l'Exposition universelle de Paris de 1937.")

        list.add(
            CarouselItem(
                imageDrawable = R.drawable.item1,
                caption =  "46d3db7a09a9484ba889146eb084401b"
            )
        )

        list.add(
            CarouselItem(
                imageDrawable = R.drawable.item2,
                caption =  "967e8834c8e746babbb8441c4f33a62b"
            )
        )
        list.add(
            CarouselItem(
                imageDrawable = R.drawable.item3,
                caption = "item3"
            )
        )
        selectedDrawable = findViewById(R.id.drawable_selected)
        carousel = findViewById(R.id.carousel)
        carousel.addData(list)
        carousel.onItemClickListener = object : OnItemClickListener {
            override fun onClick(position: Int, carouselItem: CarouselItem) {
                selectedItem = carouselItem
                selectedDrawable.visibility = View.VISIBLE
                selectedDrawable.setImageResource(carouselItem.imageDrawable!!)
            }

            override fun onLongClick(position: Int, dataObject: CarouselItem) {
                setDialog(
                    imageData[dataObject.caption]?.name,
                    imageData[dataObject.caption]?.description
                )
                alertDialog?.show()
            }
        }
        carousel.onScrollListener = object : CarouselOnScrollListener {

            override fun onScrollStateChanged(
                recyclerView: RecyclerView,
                newState: Int,
                position: Int,
                carouselItem: CarouselItem?
            ) {

            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                // ...
            }

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            var windowParams = window.getAttributes();
            windowParams.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            getWindow().setAttributes(windowParams);
        }

        mTarget = intent.getIntExtra("Target", 0)
        mVuforiaStarted = false
        mSurfaceChanged = true

        // Create an OpenGL ES 3.0 context (also works for 3.1, 3.2)
        mGLView = findViewById(R.id.surface)
        mGLView.holder.addCallback(this)
        mGLView.setEGLContextClientVersion(3)
        mGLView.setRenderer(this)

        // Hide the GLView until we are ready
        mGLView.visibility = View.GONE

        // Prevent screen from dimming
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        // Make the Activity completely full screen
        mGLView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Setup and show a progress indicator
        mProgressIndicatorLayout = View.inflate(
            applicationContext,
            R.layout.progress_indicator, null
        ) as RelativeLayout

        addContentView(
            mProgressIndicatorLayout, ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )

        // Start Vuforia initialization in a coroutine
        GlobalScope.launch(Dispatchers.Unconfined) {
            initializeVuforia()
        }

        mGestureDetector = GestureDetectorCompat(this, GestureListener())
    }


    override fun onPause() {
        pauseAR()
        super.onPause()
    }


    override fun onResume() {
        super.onResume()

        if (mVuforiaStarted) {
            GlobalScope.launch(Dispatchers.Unconfined) {
                resumeAR()
            }
        }
    }


    override fun onBackPressed() {
        stopAR()
        mVuforiaStarted = false;
        deinitAR()
        super.onBackPressed()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this)
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    // Overrider onTouchEvent to connect it to our GestureListener
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        mGestureDetector?.onTouchEvent(event)
        return super.onTouchEvent(event)
    }


    /// Custom GestureListener to capture single and double tap
    inner class GestureListener : SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent): Boolean {
            // Calls the Autofocus Native Method
            cameraPerformAutoFocus()

            for (imageTargetUniqueId in imageTargetUniqueIds) {
                if (isTapOnScreenInsideTarget(imageTargetUniqueId, e.x, e.y)) {
                    if(isIdInDiscoveredItemList(imageTargetUniqueId)) {
                        setDialog(
                            imageData[imageTargetUniqueId]?.name,
                            imageData[imageTargetUniqueId]?.description
                        )
                        alertDialog?.show()
                    } else {
                        val uniqueImageId = selectedItem?.caption;
                        if (uniqueImageId != null) {
                            if (uniqueImageId == imageTargetUniqueId) {
                                list.remove(selectedItem)
                                carousel.addData(list)
                                addToDiscoveredItemList(imageTargetUniqueId)
                                setDialog(
                                    imageData[uniqueImageId]?.name,
                                    imageData[uniqueImageId]?.description
                                )
                                alertDialog?.show()
                                selectedItem = null
                                selectedDrawable.visibility = View.INVISIBLE
                            }
                        }
                    }
                }
            }

            // After triggering a focus event wait 2 seconds
            // before restoring continuous autofocus
            Timer("RestoreAutoFocus", false).schedule(2000) {
                cameraRestoreAutoFocus()
            }

            return true
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            onBackPressed()
            return true
        }
    }


    private suspend fun initializeVuforia() {
        return GlobalScope.async(Dispatchers.Default) {
            initAR(this@VuforiaActivity, this@VuforiaActivity.assets, mTarget)
        }.await()
    }


    private fun presentError(message: String) {
        val builder: AlertDialog.Builder? = this.let {
            AlertDialog.Builder(it)
        }

        builder?.setMessage(message)
        builder?.setTitle(R.string.error_dialog_title)
        builder?.setPositiveButton(
            R.string.ok,
            DialogInterface.OnClickListener { _, _ ->
                stopAR()
                deinitAR()
                this@VuforiaActivity.finish()
            })

        // This is called from another coroutine not on the Main thread
        // Showing the UI needs to be on the main thread
        GlobalScope.launch(Dispatchers.Main) {
            val dialog: AlertDialog? = builder?.create()
            dialog?.show()
        }
    }

    private fun createDialog() {
        alertDialog = this@VuforiaActivity.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton(R.string.ok,
                    DialogInterface.OnClickListener { _, _ ->
                        // User clicked OK button
                    })
            }

            // Create the AlertDialog
            builder.create()
        }
    }

    private fun setDialog(name: String?, description: String?) {
        if (name != null && description != null) {
            alertDialog?.setTitle(name)
            alertDialog?.setMessage(description)
        }
    }

    private fun initDone() {
        mVuforiaStarted = startAR()
        if (!mVuforiaStarted) {
            Log.e("VuforiaSample", "Failed to start AR")
        }
        // Show the GLView
        GlobalScope.launch(Dispatchers.Main) {
            mGLView.visibility = View.VISIBLE
        }
    }


    // GLSurfaceView.Renderer methods
    override fun onSurfaceCreated(unused: GL10, config: EGLConfig) {
        initRendering()
    }


    override fun onDrawFrame(unused: GL10) {
        if (mVuforiaStarted) {

            if (mSurfaceChanged) {
                mSurfaceChanged = false

                // This sample doesn't support auto-rotation, landscape orientation is hard coded here
                configureRendering(mWidth, mHeight, /* Landscape Left */2)
            }

            // OpenGL rendering of Video Background and augmentations is implemented in native code
            val didRender = renderFrame()
            if (didRender && mProgressIndicatorLayout?.visibility != View.GONE) {
                GlobalScope.launch(Dispatchers.Main) {
                    mProgressIndicatorLayout?.visibility = View.GONE
                }
            }
        }
    }


    override fun onSurfaceChanged(unused: GL10, width: Int, height: Int) {
        // Store values for later use
        mWidth = width
        mHeight = height

        for(i in imageTargetUniqueIds.indices) {
            val id = resources.getIdentifier("item" + (i + 1), "drawable", packageName)
            val drawable = getDrawable(id)
            if (drawable != null) {
                val imgTexture = Texture.loadTextureFromDrawable(drawable)
                addTextures(
                    imgTexture.width, imgTexture.height, imgTexture.data!!, imageTargetUniqueIds[i]
                )
            }
        }

        // Update flag to tell us we need to update Vuforia configuration
        mSurfaceChanged = true
    }


    // SurfaceHolder.Callback
    override fun surfaceCreated(var1: SurfaceHolder?) {}


    override fun surfaceChanged(var1: SurfaceHolder?, var2: Int, var3: Int, var4: Int) {}


    override fun surfaceDestroyed(var1: SurfaceHolder?) {
        deinitRendering()
    }
}
