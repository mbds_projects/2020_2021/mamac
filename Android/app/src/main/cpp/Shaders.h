/*===============================================================================
Copyright (c) 2020 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#ifndef _VUFORIA_SHADERS_H_
#define _VUFORIA_SHADERS_H_

/////////////////////////////////////////////////////////////////////////////////////////
// texture shader: vertexTexCoord in vertex shader, texture2D sample
/////////////////////////////////////////////////////////////////////////////////////////
static const char *textureVertexShaderSrc = R"(
    attribute vec4 vertexPosition;
    attribute vec2 vertexTextureCoord;

    uniform mat4 modelViewProjectionMatrix;

    varying vec2 texCoord;

    void main()
    {
        gl_Position = modelViewProjectionMatrix * vertexPosition;
        texCoord = vertexTextureCoord;
    }
)";


static const char *textureFragmentShaderSrc = R"(
    precision mediump float;

    uniform sampler2D texSampler2D;

    varying vec2 texCoord;

    void main()
    {
        gl_FragColor = texture2D(texSampler2D, texCoord);
    }
)";

#endif // _VUFORIA_SHADERS_H_
