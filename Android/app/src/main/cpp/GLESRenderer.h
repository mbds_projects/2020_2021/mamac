/*===============================================================================
Copyright (c) 2020 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#ifndef _VUFORIA_GLESRENDERER_H_
#define _VUFORIA_GLESRENDERER_H_

#include <android/asset_manager.h>
#include <GLES3/gl31.h>
#include <GLES3/gl3ext.h>


#include <Vuforia/Image.h>
#include <Vuforia/Matrices.h>
#include <Vuforia/Vectors.h>

#include <vector>
#include <map>
#include <string>


/// Class to encapsulate OpenGLES rendering for the sample
class GLESRenderer {
public:
    /// Initialize the renderer ready for use
    bool init(AAssetManager *assetManager);

    /// Clean up objects created during rendering
    void deinit();

    void addImgTexture(int width, int height, unsigned char *bytes, std::string imageTargetUniqueId);

    /// Render the video background
    void renderVideoBackground(Vuforia::Matrix44F &projectionMatrix,
                               const float *vertices, const float *textureCoordinates,
                               const int numTriangles, const unsigned short *indices,
                               int textureUnit);

    /// Render a bounding box augmentation on an Image Target
    void renderImageTarget(Vuforia::Matrix44F &projectionMatrix,
                           Vuforia::Matrix44F &modelViewMatrix,
                           Vuforia::Matrix44F &scaledModelViewMatrix,
                           std::string imageTargetUniqueId);

private: // methods
    /// Attempt to create a texture from bytes
    int createTexture(int width, int height, unsigned char *bytes, std::string imageTargetUniqueId);

private: // data members

    // For video background rendering
    unsigned int mVbShaderProgramID = 0;
    GLint mVbVertexPositionHandle = 0;
    GLint mVbTextureCoordHandle = 0;
    GLint mVbMvpMatrixHandle = 0;
    GLint mVbTexSampler2DHandle = 0;

    // For augmentation rendering
    unsigned int mTextureShaderProgramID = 0;
    GLint mTextureVertexPositionHandle = 0;
    GLint mTextureMvpMatrixHandle = 0;
    GLint mTextureTextureCoordHandle = 0;
    GLint mTextureTexSampler2DHandle = 0;

    std::map<std::string, int> mImgTextureUnit;
};

#endif //_VUFORIA_GLESRENDERER_H_
