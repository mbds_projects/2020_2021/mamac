/*===============================================================================
Copyright (c) 2020 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#include <jni.h>

#include <AppController.h>
#include <Log.h>
#include "GLESRenderer.h"

#include <Vuforia/Tool.h>
#include <Vuforia/GLRenderer.h>

#include <GLES3/gl31.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

#include <vector>
#include <MathUtils.h>
#include <Vuforia/ImageTarget.h>
#include <Vuforia/ImageTargetResult.h>


// Cross-platform AppController providing high level Vuforia Engine operations
AppController controller;

// Struct to hold data that we need to store between calls
struct {
    JavaVM *vm = nullptr;
    jobject activity = nullptr;
    jobject assetManagerJava = nullptr;
    AAssetManager *assetManager = nullptr;
    jmethodID presentErrorMethodID = nullptr;
    jmethodID initDoneMethodID = nullptr;

    GLESRenderer renderer;
} gWrapperData;

// Screen dimension
jint screenWidth;
jint screenHeight;

// Trackable dimensions
std::map<std::string, Vuforia::Vec2F*> targetPositiveDimensions;

// Needed to calculate whether a screen tap is inside the target
std::map<std::string, Vuforia::Matrix44F> modelViewMatrix;
std::map<std::string, Vuforia::Matrix44F> inverseProjectMatrix;

std::map<std::string, bool> discoveredItem;


// JNI Implementation
#ifdef __cplusplus
extern "C"
{
#endif

JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_initAR(
        JNIEnv *env,
        jobject /* this */,
        jobject activity,
        jobject assetManager) {
    // Store the Java VM pointer so we can get a JNIEnv in callbacks
    if (env->GetJavaVM(&gWrapperData.vm) != 0) {
        return;
    }
    gWrapperData.activity = env->NewGlobalRef(activity);
    jclass clazz = env->GetObjectClass(activity);
    gWrapperData.presentErrorMethodID = env->GetMethodID(clazz, "presentError",
                                                         "(Ljava/lang/String;)V");
    gWrapperData.initDoneMethodID = env->GetMethodID(clazz, "initDone", "()V");
    env->DeleteLocalRef(clazz);

    AppController::InitConfig initConfig;
    initConfig.vuforiaInitFlags = Vuforia::INIT_FLAGS::GL_30;
    initConfig.appData = activity;

    // Setup callbacks
    initConfig.showErrorCallback = [](const char *errorString) {
        LOG("Error callback invoked. Message: %s", errorString);
        JNIEnv *env = nullptr;
        if (gWrapperData.vm->GetEnv((void **) &env, JNI_VERSION_1_6) == 0) {
            jstring error = env->NewStringUTF(errorString);
            env->CallVoidMethod(gWrapperData.activity, gWrapperData.presentErrorMethodID, error);
            env->DeleteLocalRef(error);
        }
    };
    initConfig.initDoneCallback = []() {
        LOG("InitDone callback");
        JNIEnv *env = nullptr;
        if (gWrapperData.vm->GetEnv((void **) &env, JNI_VERSION_1_6) == 0) {
            env->CallVoidMethod(gWrapperData.activity, gWrapperData.initDoneMethodID);
        }
    };

    // Get a native AAssetManager
    gWrapperData.assetManagerJava = env->NewGlobalRef(assetManager);
    gWrapperData.assetManager = AAssetManager_fromJava(env, assetManager);
    if (gWrapperData.assetManager == nullptr) {
        initConfig.showErrorCallback("Error: Failed to get the asset manager");
        return;
    }

    // Start Vuforia initialization
    controller.initAR(initConfig);
}


JNIEXPORT jboolean JNICALL
Java_com_mbds_mamac_VuforiaActivity_startAR(
        JNIEnv *env,
        jobject /* this */) {
    return controller.startAR() ? 1 : 0;
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_pauseAR(
        JNIEnv *env,
        jobject /* this */) {
    controller.pauseAR();
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_resumeAR(
        JNIEnv *env,
        jobject /* this */) {
    controller.resumeAR();
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_stopAR(
        JNIEnv *env,
        jobject /* this */) {
    controller.stopAR();
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_deinitAR(
        JNIEnv *env,
        jobject /* this */) {
    controller.deinitAR();
    discoveredItem.clear();

    env->DeleteGlobalRef(gWrapperData.assetManagerJava);
    gWrapperData.assetManagerJava = nullptr;
    gWrapperData.assetManager = nullptr;
    env->DeleteGlobalRef(gWrapperData.activity);
    gWrapperData.activity = nullptr;
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_cameraPerformAutoFocus(
        JNIEnv *env,
        jobject /* this */) {
    controller.cameraPerformAutoFocus();
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_cameraRestoreAutoFocus(
        JNIEnv *env,
        jobject /* this */) {
    controller.cameraRestoreAutoFocus();
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_initRendering(
        JNIEnv *env,
        jobject /* this */) {
    // Define clear color
    glClearColor(0.0f, 0.0f, 0.0f, Vuforia::requiresAlpha() ? 0.0f : 1.0f);

    if (!gWrapperData.renderer.init(gWrapperData.assetManager)) {
        LOG("Error initialising rendering");
    }
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_addTextures(
        JNIEnv *env,
        jobject /* this */,
        jint imgWidth, jint imgHeight, jobject imgByteBuffer, jstring imageUniqueId) {
    auto imgBytes = static_cast<unsigned char *>(env->GetDirectBufferAddress(imgByteBuffer));
    jsize length = (*env).GetStringLength(imageUniqueId);
    jboolean isCopy = JNI_FALSE;
    auto *convertedValue = static_cast<const char *>(env->GetStringUTFChars(imageUniqueId, &isCopy));
    std::string imageUniqueIdStr = std::string(convertedValue, length);
    gWrapperData.renderer.addImgTexture(imgWidth, imgHeight, imgBytes, imageUniqueIdStr);
}


JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_deinitRendering(
        JNIEnv *env,
        jobject /* this */) {
    gWrapperData.renderer.deinit();
}


JNIEXPORT jboolean JNICALL
Java_com_mbds_mamac_VuforiaActivity_configureRendering(
        JNIEnv *env,
        jobject /* this */,
        jint width, jint height,
        jint orientation) {
    screenWidth = width;
    screenHeight = height;
    return controller.configureRendering(width, height, orientation) ? 1 : 0;
}


JNIEXPORT jboolean JNICALL
Java_com_mbds_mamac_VuforiaActivity_renderFrame(
        JNIEnv *env,
        jobject /* this */) {
    if (!controller.isCameraStarted()) {
        return JNI_FALSE;
    }

    // Clear colour and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Vuforia::GLTextureUnit vbTextureUnit;
    vbTextureUnit.mTextureUnit = 0;
    double viewport[6];
    if (controller.prepareToRender(viewport, nullptr, &vbTextureUnit)) {
        // Set viewport for current view
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

        auto renderingPrimitives = controller.getRenderingPrimitives();
        Vuforia::Matrix44F vbProjectionMatrix = Vuforia::Tool::convert2GLMatrix(
                renderingPrimitives->getVideoBackgroundProjectionMatrix(Vuforia::VIEW_SINGULAR));
        const Vuforia::Mesh &vbMesh = renderingPrimitives->getVideoBackgroundMesh(
                Vuforia::VIEW_SINGULAR);
        gWrapperData.renderer.renderVideoBackground(vbProjectionMatrix,
                                                    vbMesh.getPositionCoordinates(),
                                                    vbMesh.getUVCoordinates(),
                                                    vbMesh.getNumTriangles(), vbMesh.getTriangles(),
                                                    vbTextureUnit.mTextureUnit);

        Vuforia::Matrix44F trackableProjection;
        Vuforia::Matrix44F trackableModelView;
        Vuforia::Matrix44F trackableModelViewScaled;
        std::string targetUniqueId;

        const auto &trackableResultList = controller.getImageTargetResults();
        if (!trackableResultList.empty()) {
            for (const auto *result: trackableResultList) {
                if (controller.getImageTargetResultData(
                        const_cast<Vuforia::TrackableResult *>(result), trackableProjection,
                        trackableModelView, trackableModelViewScaled, targetUniqueId)) {
                    const Vuforia::ImageTarget &target = static_cast<const Vuforia::ImageTargetResult *>(result)->getTrackable();
                    targetPositiveDimensions[targetUniqueId] = new Vuforia::Vec2F(
                            target.getSize().data[0], target.getSize().data[1]);
                    targetPositiveDimensions[targetUniqueId]->data[0] /= 2.0f;
                    targetPositiveDimensions[targetUniqueId]->data[1] /= 2.0f;

                    modelViewMatrix[targetUniqueId] = trackableModelView;

                    inverseProjectMatrix[targetUniqueId] = MathUtils::Matrix44FInverse(
                            trackableProjection);

                    // Check if image target was already discovered before rendering it
                    if (discoveredItem.find(targetUniqueId) != discoveredItem.end() && discoveredItem[targetUniqueId]) {
                        gWrapperData.renderer.renderImageTarget(trackableProjection,
                                                                trackableModelView,
                                                                trackableModelViewScaled,
                                                                targetUniqueId);
                    }
                }
            }
        }
    }

    controller.finishRender(nullptr);

    return JNI_TRUE;
}

JNIEXPORT jboolean JNICALL
Java_com_mbds_mamac_VuforiaActivity_isTapOnScreenInsideTarget(JNIEnv *env, jobject, jstring imageUniqueId, jfloat x, jfloat y)
{
    //LOG("Java_com_qualcomm_QCARSamples_VideoPlayback_VideoPlayback_isTapOnScreenInsideTarget");
    // Here we calculate that the touch event is inside the target
    Vuforia::Vec3F intersection, lineStart, lineEnd;
    jsize length = (*env).GetStringLength(imageUniqueId);
    jboolean isCopy = JNI_FALSE;
    auto *convertedValue = static_cast<const char *>(env->GetStringUTFChars(imageUniqueId, &isCopy));
    std::string imageUniqueIdStr = std::string(convertedValue, length);


    if (inverseProjectMatrix.find(imageUniqueIdStr) != inverseProjectMatrix.end() &&
        modelViewMatrix.find(imageUniqueIdStr) != modelViewMatrix.end() &&
        targetPositiveDimensions.find(imageUniqueIdStr) != targetPositiveDimensions.end()) {
        MathUtils::projectScreenPointToPlane(inverseProjectMatrix[imageUniqueIdStr],
                                             modelViewMatrix[imageUniqueIdStr], screenWidth,
                                             screenHeight,
                                             Vuforia::Vec2F(x, y), Vuforia::Vec3F(0, 0, 0),
                                             Vuforia::Vec3F(0, 0, 1), intersection, lineStart,
                                             lineEnd);

        // The target returns as pose the center of the trackable. The following if-statement simply checks that the tap is within this range
        return (intersection.data[0] >= -(targetPositiveDimensions[imageUniqueIdStr]->data[0])) &&
               (intersection.data[0] <= (targetPositiveDimensions[imageUniqueIdStr]->data[0])) &&
               (intersection.data[1] >= -(targetPositiveDimensions[imageUniqueIdStr]->data[1])) &&
               (intersection.data[1] <= (targetPositiveDimensions[imageUniqueIdStr]->data[1]));
    }
    return false;
}

JNIEXPORT void JNICALL
Java_com_mbds_mamac_VuforiaActivity_addToDiscoveredItemList(JNIEnv *env, jobject, jstring imageUniqueId)
{
    jsize length = (*env).GetStringLength(imageUniqueId);
    jboolean isCopy = JNI_FALSE;
    auto *convertedValue = static_cast<const char *>(env->GetStringUTFChars(imageUniqueId, &isCopy));
    std::string imageUniqueIdStr = std::string(convertedValue, length);

    discoveredItem[imageUniqueIdStr] = true;
}

JNIEXPORT jboolean JNICALL
Java_com_mbds_mamac_VuforiaActivity_isIdInDiscoveredItemList(JNIEnv *env, jobject, jstring imageUniqueId)
{
    jsize length = (*env).GetStringLength(imageUniqueId);
    jboolean isCopy = JNI_FALSE;
    auto *convertedValue = static_cast<const char *>(env->GetStringUTFChars(imageUniqueId, &isCopy));
    std::string imageUniqueIdStr = std::string(convertedValue, length);

    return discoveredItem.find(imageUniqueIdStr) != discoveredItem.end();
}

#ifdef __cplusplus
}
#endif