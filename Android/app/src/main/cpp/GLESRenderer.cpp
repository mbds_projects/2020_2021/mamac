/*===============================================================================
Copyright (c) 2020 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#include "GLESRenderer.h"

#include "GLESUtils.h"
#include "Shaders.h"

#include <MathUtils.h>
#include <Models.h>
#include <Vuforia/Tool.h>

#include <android/asset_manager.h>

bool GLESRenderer::init(AAssetManager *assetManager) {
    // Setup for Video Background rendering
    mVbShaderProgramID =
            GLESUtils::createProgramFromBuffer(textureVertexShaderSrc, textureFragmentShaderSrc);
    mVbVertexPositionHandle =
            glGetAttribLocation(mVbShaderProgramID, "vertexPosition");
    mVbTextureCoordHandle =
            glGetAttribLocation(mVbShaderProgramID, "vertexTextureCoord");
    mVbMvpMatrixHandle =
            glGetUniformLocation(mVbShaderProgramID, "modelViewProjectionMatrix");
    mVbTexSampler2DHandle =
            glGetUniformLocation(mVbShaderProgramID, "texSampler2D");

    // Setup for augmentation rendering
    mTextureShaderProgramID =
            GLESUtils::createProgramFromBuffer(textureVertexShaderSrc, textureFragmentShaderSrc);
    mTextureVertexPositionHandle =
            glGetAttribLocation(mTextureShaderProgramID, "vertexPosition");
    mTextureMvpMatrixHandle =
            glGetUniformLocation(mTextureShaderProgramID, "modelViewProjectionMatrix");
    mTextureTexSampler2DHandle =
            glGetUniformLocation(mTextureShaderProgramID, "texSampler2D");
    mTextureTextureCoordHandle =
            glGetAttribLocation(mTextureShaderProgramID, "vertexTextureCoord");

    return true;
}


void GLESRenderer::deinit() {
    for (auto &keyValue : mImgTextureUnit) {
        if (keyValue.second != -1) {
            GLESUtils::destroyTexture(keyValue.second);
            keyValue.second = -1;
        }
    }
}

void GLESRenderer::renderVideoBackground(
        Vuforia::Matrix44F &projectionMatrix,
        const float *vertices, const float *textureCoordinates,
        const int numTriangles, const unsigned short *indices,
        int textureUnit) {
    GLboolean depthTest = GL_FALSE;
    GLboolean cullTest = GL_FALSE;

    glGetBooleanv(GL_DEPTH_TEST, &depthTest);
    glGetBooleanv(GL_CULL_FACE, &cullTest);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // Load the shader and upload the vertex/texcoord/index data
    glUseProgram(mVbShaderProgramID);
    glVertexAttribPointer(static_cast<GLuint>(mVbVertexPositionHandle), 3, GL_FLOAT,
                          GL_FALSE, 0, vertices);
    glVertexAttribPointer(static_cast<GLuint>(mVbTextureCoordHandle), 2, GL_FLOAT,
                          GL_FALSE, 0, textureCoordinates);

    glUniform1i(mVbTexSampler2DHandle, textureUnit);

    // Render the video background with the custom shader
    // First, we enable the vertex arrays
    glEnableVertexAttribArray(static_cast<GLuint>(mVbVertexPositionHandle));
    glEnableVertexAttribArray(static_cast<GLuint>(mVbTextureCoordHandle));

    // Pass the projection matrix to OpenGL
    glUniformMatrix4fv(mVbMvpMatrixHandle, 1, GL_FALSE, projectionMatrix.data);

    // Then, we issue the render call
    glDrawElements(GL_TRIANGLES, numTriangles * 3, GL_UNSIGNED_SHORT,
                   indices);

    // Finally, we disable the vertex arrays
    glDisableVertexAttribArray(static_cast<GLuint>(mVbVertexPositionHandle));
    glDisableVertexAttribArray(static_cast<GLuint>(mVbTextureCoordHandle));

    if (depthTest)
        glEnable(GL_DEPTH_TEST);

    if (cullTest)
        glEnable(GL_CULL_FACE);

    GLESUtils::checkGlError("Render video background");
}

void GLESRenderer::renderImageTarget(Vuforia::Matrix44F &projectionMatrix,
                                     Vuforia::Matrix44F &modelViewMatrix,
                                     Vuforia::Matrix44F &scaledModelViewMatrix,
                                     std::string imageTargetUniqueId) {
    Vuforia::Matrix44F scaledModelViewProjectionMatrix;
    MathUtils::multiplyMatrix(projectionMatrix, scaledModelViewMatrix,
                              scaledModelViewProjectionMatrix);

    mTextureVertexPositionHandle =
            glGetAttribLocation(mTextureShaderProgramID, "vertexPosition");
    mTextureMvpMatrixHandle =
            glGetUniformLocation(mTextureShaderProgramID, "modelViewProjectionMatrix");
    mTextureTexSampler2DHandle =
            glGetUniformLocation(mTextureShaderProgramID, "texSampler2D");
    mTextureTextureCoordHandle =
            glGetAttribLocation(mTextureShaderProgramID, "vertexTextureCoord");

    glDisable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, mImgTextureUnit[imageTargetUniqueId]);

    glEnableVertexAttribArray(mTextureVertexPositionHandle);
    glVertexAttribPointer(mTextureVertexPositionHandle, 3, GL_FLOAT, GL_FALSE, 0,
                          (const GLvoid *) &squareVertices[0]);

    glEnableVertexAttribArray(mTextureTextureCoordHandle);
    glVertexAttribPointer(mTextureTextureCoordHandle, 2, GL_FLOAT, GL_FALSE, 0,
                          (const GLvoid *) &squareTexCoords[0]);

    glUseProgram(mTextureShaderProgramID);
    glUniformMatrix4fv(mTextureMvpMatrixHandle, 1, GL_FALSE,
                       (GLfloat *) scaledModelViewProjectionMatrix.data);
    glUniform1i(mTextureTexSampler2DHandle, 0); //texture unit, not handle

    // Draw
    glDrawElements(GL_TRIANGLES, NUM_SQUARE_INDEX, GL_UNSIGNED_SHORT,
                   (const GLvoid *) &squareIndices[0]);

    //disable input data structures
    glDisableVertexAttribArray(mTextureVertexPositionHandle);
    glDisableVertexAttribArray(mTextureVertexPositionHandle);
    glUseProgram(0);

    glBindTexture(GL_TEXTURE_2D, 0);


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
}

void GLESRenderer::addImgTexture(int width, int height, unsigned char *bytes, std::string imageTargetUniqueId) {
    mImgTextureUnit[imageTargetUniqueId] = createTexture(width, height, bytes, imageTargetUniqueId);
}

int GLESRenderer::createTexture(int width, int height, unsigned char *bytes, std::string imageTargetUniqueId) {
    if (mImgTextureUnit.find(imageTargetUniqueId) != mImgTextureUnit.end()) {
        GLESUtils::destroyTexture(mImgTextureUnit[imageTargetUniqueId]);
    }
    return GLESUtils::createTexture(width, height, bytes);
}
