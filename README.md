# MAMAC AR
Authors : BAROUKH Yoni – CHOISY Jérémy – PANZERA Alexis

This is the MAMAC AR app using the Vuforia Engine API.

The goal of this app is to map an image target to be recognized and tracked by the Vuforia Engine with an actual picture stored in the app.
The user can interact with those pictures through a carousel located at the bottom part of the screen. Once the user selects one picture, he can touch the different image targets and if one of them is mapped with the selected picture, the image target will be augmented with it.

## Getting Started

To build and run the sample you will need to download the Vuforia Engine SDK package and unpack it onto your computer.
Once you have the Vuforia Engine you will find a 'samples' directory where you should clone this project.

Your directory structure will look like this:

```
vuforia-sdk
  |-- samples
        |-- mamac-ar
              |-- Assets
              |-- CrossPlatform
              |-- Media
              |-- Android
              |-- README.md
              |-- license.txt
```

### Prerequisites

- Android Studio
- You will need to install the Android SDK Tools ```CMake```
- An account on the Vuforia Engine developer portal (http://developer.vuforia.com)

### Building the app

1. Launch your chosen IDE.
2. Open the project.
3. Edit the source file CrossPlatform/AppController.cpp and add your license key from the Vuforia Engine developer portal.
4. Build & Run.

### Use the app with new image targets and associated pictures

#### Configure the image targets

1. Log in on the Vuforia Engine developer portal.
2. Go to Develop -> Target Manager and create a new database.
3. Add the image targets to your database.
4. Download your database when you are done, you'll get two files (.xml and .dat).
5. Replace the already existing database files in ```Assets/ImageTargets/``` by yours.
6. Edit the source file ```CrossPlatform/AppController.cpp``` and update the .xml file's name with yours there :
```
mCurrentDataSet = loadAndActivateDataSet("MAMAC.xml");
```
7. Edit the source file VuforiaActivity.kt and update the ```imageTargetUniqueIds``` with the unique ID of each image target (you can find them in the Target Manager on the Vuforia Engine developer portal).

#### Map the associated pictures
1. Remove the ```item*.png``` from the `drawable` directory.
2. Add your pictures there instead, following the same naming convention (```item1```,```item2```...).
3. Edit the source file VuforiaActivity.kt :
     - update the code adding the pictures to the carousel in order to add your own pictures with the unique ID of the associated image target as the ```caption```.
     - populate the map ```imageData``` with the unique ID of the associated image target as the key and a ```CarouselItemData``` object as the value. This object will contain the name of the picture and a description, it will be used in the dialogs. 
