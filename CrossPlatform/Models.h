/*===============================================================================
Copyright (c) 2020 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#ifndef _VUFORIA_MODELS_H_
#define _VUFORIA_MODELS_H_


// SQUARE MODEL
static const unsigned short NUM_SQUARE_VERTEX = 4;
static const unsigned short NUM_SQUARE_INDEX = 6;

static const float squareVertices[NUM_SQUARE_VERTEX * 3] =
        {
                -0.50f, -0.50f, 0.00f,
                0.50f, -0.50f, 0.00f,
                0.50f, 0.50f, 0.00f,
                -0.50f, 0.50f, 0.00f
        };

static const float squareTexCoords[NUM_SQUARE_VERTEX * 2] =
        {
                0.0f, 1.0f,
                1.0f, 1.0f,
                1.0f, 0.0f,
                0.0f, 0.0f
        };

static const unsigned short squareIndices[NUM_SQUARE_INDEX] =
        {
                0, 1, 2, 0, 2, 3
        };

// END SQUARE MODEL

#endif //_VUFORIA_MODELS_H_
