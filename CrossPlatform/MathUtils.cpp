/*===============================================================================
Copyright (c) 2020, PTC Inc. All rights reserved.
 
Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#include "MathUtils.h"
#include "Log.h"

#define _USE_MATH_DEFINES

#include <cmath>
#include <Vuforia/Renderer.h>
#include <Vuforia/VideoBackgroundConfig.h>

Vuforia::Vec3F
MathUtils::Vec3FAdd(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2) {
    Vuforia::Vec3F r;

    r.data[0] = v1.data[0] + v2.data[0];
    r.data[1] = v1.data[1] + v2.data[1];
    r.data[2] = v1.data[2] + v2.data[2];

    return r;
}


Vuforia::Vec3F
MathUtils::Vec3FSub(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2) {
    Vuforia::Vec3F r;

    r.data[0] = v1.data[0] - v2.data[0];
    r.data[1] = v1.data[1] - v2.data[1];
    r.data[2] = v1.data[2] - v2.data[2];

    return r;
}

Vuforia::Vec3F
MathUtils::Vec3FScale(const Vuforia::Vec3F &v, float s) {
    Vuforia::Vec3F r;

    r.data[0] = v.data[0] * s;
    r.data[1] = v.data[1] * s;
    r.data[2] = v.data[2] * s;

    return r;
}


float
MathUtils::Vec3FDot(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2) {
    return v1.data[0] * v2.data[0] + v1.data[1] * v2.data[1] + v1.data[2] * v2.data[2];
}

Vuforia::Vec3F
MathUtils::Vec3FNormalize(const Vuforia::Vec3F &v) {
    Vuforia::Vec3F r;

    float length = sqrtf(v.data[0] * v.data[0] + v.data[1] * v.data[1] + v.data[2] * v.data[2]);
    if (length != 0.0f)
        length = 1.0f / length;

    r.data[0] = v.data[0] * length;
    r.data[1] = v.data[1] * length;
    r.data[2] = v.data[2] * length;

    return r;
}

Vuforia::Vec4F
MathUtils::Vec4FTransform2(Vuforia::Vec4F& v, Vuforia::Matrix44F& m)
{
    Vuforia::Vec4F r;

    r.data[0] = m.data[0] * v.data[0] +
                m.data[1] * v.data[1] +
                m.data[2] * v.data[2] +
                m.data[3] * v.data[3];
    r.data[1] = m.data[4] * v.data[0] +
                m.data[5] * v.data[1] +
                m.data[6] * v.data[2] +
                m.data[7] * v.data[3];
    r.data[2] = m.data[8] * v.data[0] +
                m.data[9] * v.data[1] +
                m.data[10] * v.data[2] +
                m.data[11] * v.data[3];
    r.data[3] = m.data[12] * v.data[0] +
                m.data[13] * v.data[1] +
                m.data[14] * v.data[2] +
                m.data[15] * v.data[3];

    return r;
}

Vuforia::Matrix44F
MathUtils::Matrix44FTranspose(const Vuforia::Matrix44F &m) {
    Vuforia::Matrix44F r;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            r.data[i * 4 + j] = m.data[i + 4 * j];
    return r;
}


float
MathUtils::Matrix44FDeterminate(const Vuforia::Matrix44F &m) {
    return m.data[12] * m.data[9] * m.data[6] * m.data[3] -
           m.data[8] * m.data[13] * m.data[6] * m.data[3] -
           m.data[12] * m.data[5] * m.data[10] * m.data[3] +
           m.data[4] * m.data[13] * m.data[10] * m.data[3] +
           m.data[8] * m.data[5] * m.data[14] * m.data[3] -
           m.data[4] * m.data[9] * m.data[14] * m.data[3] -
           m.data[12] * m.data[9] * m.data[2] * m.data[7] +
           m.data[8] * m.data[13] * m.data[2] * m.data[7] +
           m.data[12] * m.data[1] * m.data[10] * m.data[7] -
           m.data[0] * m.data[13] * m.data[10] * m.data[7] -
           m.data[8] * m.data[1] * m.data[14] * m.data[7] +
           m.data[0] * m.data[9] * m.data[14] * m.data[7] +
           m.data[12] * m.data[5] * m.data[2] * m.data[11] -
           m.data[4] * m.data[13] * m.data[2] * m.data[11] -
           m.data[12] * m.data[1] * m.data[6] * m.data[11] +
           m.data[0] * m.data[13] * m.data[6] * m.data[11] +
           m.data[4] * m.data[1] * m.data[14] * m.data[11] -
           m.data[0] * m.data[5] * m.data[14] * m.data[11] -
           m.data[8] * m.data[5] * m.data[2] * m.data[15] +
           m.data[4] * m.data[9] * m.data[2] * m.data[15] +
           m.data[8] * m.data[1] * m.data[6] * m.data[15] -
           m.data[0] * m.data[9] * m.data[6] * m.data[15] -
           m.data[4] * m.data[1] * m.data[10] * m.data[15] +
           m.data[0] * m.data[5] * m.data[10] * m.data[15];
}


Vuforia::Matrix44F
MathUtils::Matrix44FInverse(const Vuforia::Matrix44F &m) {
    Vuforia::Matrix44F r;

    float det = 1.0f / Matrix44FDeterminate(m);

    r.data[0] = m.data[6] * m.data[11] * m.data[13] - m.data[7] * m.data[10] * m.data[13]
                + m.data[7] * m.data[9] * m.data[14] - m.data[5] * m.data[11] * m.data[14]
                - m.data[6] * m.data[9] * m.data[15] + m.data[5] * m.data[10] * m.data[15];

    r.data[4] = m.data[3] * m.data[10] * m.data[13] - m.data[2] * m.data[11] * m.data[13]
                - m.data[3] * m.data[9] * m.data[14] + m.data[1] * m.data[11] * m.data[14]
                + m.data[2] * m.data[9] * m.data[15] - m.data[1] * m.data[10] * m.data[15];

    r.data[8] = m.data[2] * m.data[7] * m.data[13] - m.data[3] * m.data[6] * m.data[13]
                + m.data[3] * m.data[5] * m.data[14] - m.data[1] * m.data[7] * m.data[14]
                - m.data[2] * m.data[5] * m.data[15] + m.data[1] * m.data[6] * m.data[15];

    r.data[12] = m.data[3] * m.data[6] * m.data[9] - m.data[2] * m.data[7] * m.data[9]
                 - m.data[3] * m.data[5] * m.data[10] + m.data[1] * m.data[7] * m.data[10]
                 + m.data[2] * m.data[5] * m.data[11] - m.data[1] * m.data[6] * m.data[11];

    r.data[1] = m.data[7] * m.data[10] * m.data[12] - m.data[6] * m.data[11] * m.data[12]
                - m.data[7] * m.data[8] * m.data[14] + m.data[4] * m.data[11] * m.data[14]
                + m.data[6] * m.data[8] * m.data[15] - m.data[4] * m.data[10] * m.data[15];

    r.data[5] = m.data[2] * m.data[11] * m.data[12] - m.data[3] * m.data[10] * m.data[12]
                + m.data[3] * m.data[8] * m.data[14] - m.data[0] * m.data[11] * m.data[14]
                - m.data[2] * m.data[8] * m.data[15] + m.data[0] * m.data[10] * m.data[15];

    r.data[9] = m.data[3] * m.data[6] * m.data[12] - m.data[2] * m.data[7] * m.data[12]
                - m.data[3] * m.data[4] * m.data[14] + m.data[0] * m.data[7] * m.data[14]
                + m.data[2] * m.data[4] * m.data[15] - m.data[0] * m.data[6] * m.data[15];

    r.data[13] = m.data[2] * m.data[7] * m.data[8] - m.data[3] * m.data[6] * m.data[8]
                 + m.data[3] * m.data[4] * m.data[10] - m.data[0] * m.data[7] * m.data[10]
                 - m.data[2] * m.data[4] * m.data[11] + m.data[0] * m.data[6] * m.data[11];

    r.data[2] = m.data[5] * m.data[11] * m.data[12] - m.data[7] * m.data[9] * m.data[12]
                + m.data[7] * m.data[8] * m.data[13] - m.data[4] * m.data[11] * m.data[13]
                - m.data[5] * m.data[8] * m.data[15] + m.data[4] * m.data[9] * m.data[15];

    r.data[6] = m.data[3] * m.data[9] * m.data[12] - m.data[1] * m.data[11] * m.data[12]
                - m.data[3] * m.data[8] * m.data[13] + m.data[0] * m.data[11] * m.data[13]
                + m.data[1] * m.data[8] * m.data[15] - m.data[0] * m.data[9] * m.data[15];

    r.data[10] = m.data[1] * m.data[7] * m.data[12] - m.data[3] * m.data[5] * m.data[12]
                 + m.data[3] * m.data[4] * m.data[13] - m.data[0] * m.data[7] * m.data[13]
                 - m.data[1] * m.data[4] * m.data[15] + m.data[0] * m.data[5] * m.data[15];

    r.data[14] = m.data[3] * m.data[5] * m.data[8] - m.data[1] * m.data[7] * m.data[8]
                 - m.data[3] * m.data[4] * m.data[9] + m.data[0] * m.data[7] * m.data[9]
                 + m.data[1] * m.data[4] * m.data[11] - m.data[0] * m.data[5] * m.data[11];

    r.data[3] = m.data[6] * m.data[9] * m.data[12] - m.data[5] * m.data[10] * m.data[12]
                - m.data[6] * m.data[8] * m.data[13] + m.data[4] * m.data[10] * m.data[13]
                + m.data[5] * m.data[8] * m.data[14] - m.data[4] * m.data[9] * m.data[14];

    r.data[7] = m.data[1] * m.data[10] * m.data[12] - m.data[2] * m.data[9] * m.data[12]
                + m.data[2] * m.data[8] * m.data[13] - m.data[0] * m.data[10] * m.data[13]
                - m.data[1] * m.data[8] * m.data[14] + m.data[0] * m.data[9] * m.data[14];

    r.data[11] = m.data[2] * m.data[5] * m.data[12] - m.data[1] * m.data[6] * m.data[12]
                 - m.data[2] * m.data[4] * m.data[13] + m.data[0] * m.data[6] * m.data[13]
                 + m.data[1] * m.data[4] * m.data[14] - m.data[0] * m.data[5] * m.data[14];

    r.data[15] = m.data[1] * m.data[6] * m.data[8] - m.data[2] * m.data[5] * m.data[8]
                 + m.data[2] * m.data[4] * m.data[9] - m.data[0] * m.data[6] * m.data[9]
                 - m.data[1] * m.data[4] * m.data[10] + m.data[0] * m.data[5] * m.data[10];

    for (int i = 0; i < 16; i++)
        r.data[i] *= det;

    return r;
}

Vuforia::Matrix44F
MathUtils::Matrix44FScale(const Vuforia::Vec3F &scale, const Vuforia::Matrix44F &m) {
    Vuforia::Matrix44F r;

    r = copyMatrix(m);
    scaleMatrix(scale, r);

    return r;
}

Vuforia::Matrix44F
MathUtils::copyMatrix(const Vuforia::Matrix44F &m) {
    return m;
}

void
MathUtils::scaleMatrix(const Vuforia::Vec3F &scale, Vuforia::Matrix44F &m) {
    //m =  m * scale_m
    m.data[0] *= scale.data[0];
    m.data[1] *= scale.data[0];
    m.data[2] *= scale.data[0];
    m.data[3] *= scale.data[0];

    m.data[4] *= scale.data[1];
    m.data[5] *= scale.data[1];
    m.data[6] *= scale.data[1];
    m.data[7] *= scale.data[1];

    m.data[8] *= scale.data[2];
    m.data[9] *= scale.data[2];
    m.data[10] *= scale.data[2];
    m.data[11] *= scale.data[2];
}


void
MathUtils::multiplyMatrix(const Vuforia::Matrix44F &matrixA, const Vuforia::Matrix44F &matrixB,
                          Vuforia::Matrix44F &matrixC) {
    int i, j, k;
    Vuforia::Matrix44F aTmp;

    // matrixC= matrixA * matrixB
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            aTmp.data[j * 4 + i] = 0.0;

            for (k = 0; k < 4; k++)
                aTmp.data[j * 4 + i] += matrixA.data[k * 4 + i] * matrixB.data[j * 4 + k];
        }
    }

    for (i = 0; i < 16; i++)
        matrixC.data[i] = aTmp.data[i];
}

Vuforia::Vec4F
MathUtils::Vec4FDiv(Vuforia::Vec4F v, float s)
{
    Vuforia::Vec4F r;
    r.data[0] = v.data[0] / s;
    r.data[1] = v.data[1] / s;
    r.data[2] = v.data[2] / s;
    r.data[3] = v.data[3] / s;
    return r;
}

bool
MathUtils::linePlaneIntersection(Vuforia::Vec3F lineStart, Vuforia::Vec3F lineEnd,
                      Vuforia::Vec3F pointOnPlane, Vuforia::Vec3F planeNormal,
                      Vuforia::Vec3F &intersection)
{
    Vuforia::Vec3F lineDir = Vec3FSub(lineEnd, lineStart);
    lineDir = Vec3FNormalize(lineDir);

    Vuforia::Vec3F planeDir = Vec3FSub(pointOnPlane, lineStart);

    float n = Vec3FDot(planeNormal, planeDir);
    float d = Vec3FDot(planeNormal, lineDir);

    if (fabs(d) < 0.00001) {
        // Line is parallel to plane
        return false;
    }

    float dist = n / d;

    Vuforia::Vec3F offset = Vec3FScale(lineDir, dist);
    intersection = Vec3FAdd(lineStart, offset);

    return true;
}


void
MathUtils::projectScreenPointToPlane(Vuforia::Matrix44F inverseProjMatrix, Vuforia::Matrix44F modelViewMatrix,
                          float screenWidth, float screenHeight,
                          Vuforia::Vec2F point, Vuforia::Vec3F planeCenter, Vuforia::Vec3F planeNormal,
                          Vuforia::Vec3F &intersection, Vuforia::Vec3F &lineStart, Vuforia::Vec3F &lineEnd)
{
    // Window Coordinates to Normalized Device Coordinates
    Vuforia::VideoBackgroundConfig config = Vuforia::Renderer::getInstance().getVideoBackgroundConfig();

    float halfScreenWidth = screenWidth / 2.0f;
    float halfScreenHeight = screenHeight / 2.0f;

    float halfViewportWidth = config.mSize.data[0] / 2.0f;
    float halfViewportHeight = config.mSize.data[1] / 2.0f;

    float x = (point.data[0] - halfScreenWidth) / halfViewportWidth;
    float y = (point.data[1] - halfScreenHeight) / halfViewportHeight * -1;

    Vuforia::Vec4F ndcNear(x, y, -1, 1);
    Vuforia::Vec4F ndcFar(x, y, 1, 1);

    // Normalized Device Coordinates to Eye Coordinates
    Vuforia::Vec4F pointOnNearPlane = Vec4FTransform2(ndcNear, inverseProjMatrix);
    Vuforia::Vec4F pointOnFarPlane = Vec4FTransform2(ndcFar, inverseProjMatrix);
    pointOnNearPlane = Vec4FDiv(pointOnNearPlane, pointOnNearPlane.data[3]);
    pointOnFarPlane = Vec4FDiv(pointOnFarPlane, pointOnFarPlane.data[3]);

    // Eye Coordinates to Object Coordinates
    Vuforia::Matrix44F inverseModelViewMatrix = Matrix44FInverse(modelViewMatrix);

    Vuforia::Vec4F nearWorld = Vec4FTransform2(pointOnNearPlane, inverseModelViewMatrix);
    Vuforia::Vec4F farWorld = Vec4FTransform2(pointOnFarPlane, inverseModelViewMatrix);

    lineStart = Vuforia::Vec3F(nearWorld.data[0], nearWorld.data[1], nearWorld.data[2]);
    lineEnd = Vuforia::Vec3F(farWorld.data[0], farWorld.data[1], farWorld.data[2]);
    linePlaneIntersection(lineStart, lineEnd, planeCenter, planeNormal, intersection);
}