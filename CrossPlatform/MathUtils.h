/*===============================================================================
Copyright (c) 2020, PTC Inc. All rights reserved.
 
Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
===============================================================================*/

#ifndef __MATH_UTILS_H__
#define __MATH_UTILS_H__

#include <Vuforia/Vectors.h>
#include <Vuforia/Matrices.h>

/// Utility class for Math operations.
/**
 *
 * Provide a set of linear algebra operations for Vufoira vector and matrix.
 * All 4x4 matrix transformation consider row storage
 */
class MathUtils {
public:
    /// Create the opposite 3D vector and return the result (result = -v)
    static Vuforia::Vec3F Vec3FOpposite(const Vuforia::Vec3F &v);

    /// Addition two 3D vectors and return the result (result = v1 + v2)
    static Vuforia::Vec3F Vec3FAdd(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2);

    /// Subtract two 3D vectors and return the result (result = v1 - v2)
    static Vuforia::Vec3F Vec3FSub(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2);

    /// Multiply 3D vector by a scalar and return the result (result = v * s)
    static Vuforia::Vec3F Vec3FScale(const Vuforia::Vec3F &v, float s);

    /// Compute the dot product of two 3D vectors and return the result (result = v1.v2)
    static float Vec3FDot(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2);

    /// Compute the cross product of two 3D vectors and return the result (result = v1 x v2)
    static Vuforia::Vec3F Vec3FCross(const Vuforia::Vec3F &v1, const Vuforia::Vec3F &v2);

    /// Normalize a 3D vector and return the result (result = v/||v||)
    static Vuforia::Vec3F Vec3FNormalize(const Vuforia::Vec3F &v);

    // COMPOSITION METHODS (can be chained)

    /// Return an identify 4x4 matrix
    static Vuforia::Matrix44F Matrix44FIdentity();

    /// Transpose a 4x4 matrix and return the result (result = transpose(m))
    static Vuforia::Matrix44F Matrix44FTranspose(const Vuforia::Matrix44F &m);

    /// Compute the determinate of a 4x4 matrix and return the result ( result = det(m) )
    static float Matrix44FDeterminate(const Vuforia::Matrix44F &m);

    /// Computer the inverse of the matrix and return the result ( result = inverse(m) )
    static Vuforia::Matrix44F Matrix44FInverse(const Vuforia::Matrix44F &m);

    /// Scale the matrix m by a vector scale and return the result (post-multiply, result = M * S (scale) )
    static Vuforia::Matrix44F
    Matrix44FScale(const Vuforia::Vec3F &scale, const Vuforia::Matrix44F &m);

    /// Create copy of the 4x4 matrix and return the result
    static Vuforia::Matrix44F copyMatrix(const Vuforia::Matrix44F &m);

    // ARGUMENT METHODS (result always returned in argument)

    /// Create a rotation matrix with the axis/angle rotation
    /// Angle is in degrees
    static void makeRotationMatrix(float angle, const Vuforia::Vec3F &axis, Vuforia::Matrix44F &m);


    /// Scale the matrix m by a vector scale and return the result (post-multiply, result = M * S (scale) )
    static void scaleMatrix(const Vuforia::Vec3F &scale, Vuforia::Matrix44F &m);

    /// Multiply the two matrices A and B and writes the result to C (C = mA*mB)
    static void multiplyMatrix(const Vuforia::Matrix44F &mA, const Vuforia::Matrix44F &mB,
                               Vuforia::Matrix44F &mC);

    static void projectScreenPointToPlane(Vuforia::Matrix44F inverseProjMatrix, Vuforia::Matrix44F modelViewMatrix,
                                          float screenWidth, float screenHeight,
                                          Vuforia::Vec2F point, Vuforia::Vec3F planeCenter, Vuforia::Vec3F planeNormal,
                                          Vuforia::Vec3F &intersection, Vuforia::Vec3F &lineStart, Vuforia::Vec3F &lineEnd);
    static bool linePlaneIntersection(Vuforia::Vec3F lineStart, Vuforia::Vec3F lineEnd,
                                      Vuforia::Vec3F pointOnPlane, Vuforia::Vec3F planeNormal,
                                      Vuforia::Vec3F &intersection);
    static Vuforia::Vec4F Vec4FDiv(Vuforia::Vec4F v, float s);
    static Vuforia::Vec4F Vec4FTransform2(Vuforia::Vec4F& v, Vuforia::Matrix44F& m);
};


#endif  // __MATH_UTILS_H__
